import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  shownMenu: boolean = false;
  serverDate: any;

  constructor( private http: HttpClient) { }

  ngOnInit() {

    this.http.get( environment.server + '/date').subscribe( date => this.serverDate = date );

  }

}
