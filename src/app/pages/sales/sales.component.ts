import { Component, OnInit } from '@angular/core';
import { SalesService } from 'src/app/services/sales.service';
import { Sale } from 'src/app/models/sale';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  constructor( private _sales: SalesService ) { }

  sales: Sale[];

  ngOnInit() {

    this._sales.findAll().subscribe( sales => {
      this.sales = sales;
    });

  }

}
