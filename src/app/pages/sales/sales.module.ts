import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesComponent } from './sales.component';
import { SalesRoutingModule } from './sales-routing.module';
import { SalesFormComponent } from './sales-form/sales-form.component';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { ItemsService } from 'src/app/services/items.service';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { SalesService } from 'src/app/services/sales.service';



@NgModule({
  declarations: [SalesComponent, SalesFormComponent],
  imports: [
    CommonModule,
    SalesRoutingModule,
    PipesModule,
    ComponentsModule,
    SweetAlert2Module,
    FormsModule
  ], providers: [ ItemsService, ConfigurationService, SalesService ]
})
export class SalesModule { }
