import { Component, OnInit, ViewChild, ViewChildren, ElementRef } from '@angular/core';
import { Client } from 'src/app/models/client.model';
import { Item } from 'src/app/models/item.model';
import { SearcherComponent } from 'src/app/components/searcher/searcher.component';
import { ItemsService } from 'src/app/services/items.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Configuration } from 'src/app/models/configuration.model';
import { Router } from '@angular/router';
import { SalesService } from 'src/app/services/sales.service';

declare const $: any;

@Component({
  selector: 'app-sales-form',
  templateUrl: './sales-form.component.html',
  styleUrls: ['./sales-form.component.scss']
})
export class SalesFormComponent implements OnInit {

  @ViewChild('noStockSwal', {static: false}) private noStockSwal: SwalComponent;
  @ViewChild('duplicateItemSwal', {static: false}) private duplicateItemSwal: SwalComponent;
  @ViewChild('configSwal', {static: false}) private configSwal: SwalComponent;
  @ViewChild('nullCountSwal', {static: false}) private nullCountSwal: SwalComponent;
  @ViewChild('invalidNextSwal', {static: false}) private invalidNextSwal: SwalComponent;
  @ViewChild('successSwal', {static: false}) private successSwal: SwalComponent;

  @ViewChild('clientSearcher', {static: false}) clientSearcher: SearcherComponent;
  @ViewChild('itemSearcher', {static: false}) itemSearcher: SearcherComponent;

  @ViewChildren('itemsRows') itemsRows: any;

  constructor( 
    private _sales: SalesService,
    private _items: ItemsService, 
    private _configuration: ConfigurationService,
    private router: Router
  ) { }

  client: Client;
  item: Item;
  items: Item[] = [];
  configuration: Configuration;
  code: number;

  downpayment = 0;
  downpaymentBonus = 0;
  total = 0;
  spotPrice = 0;
  totalToPay = 0;
  payments = [];
  parcialPayments: any;

  isSecondStep: boolean;

  ngOnInit( ) {

    this.getLastId();

    this._configuration.findOne(1).subscribe( configuration => {

      if( !configuration ) {
        this.configSwal.fire();
        return;
      } else {
        this.configuration = <Configuration>configuration;
      }
      
      
    });

  }

  selectClient( client ) {

    this.client = client;

    let code = this.client.id.toString();

    if( code.length < 4 ) {

      let ceros = 4 - code.length;

      for (let i = 0; i <  ceros; i++) {
        code = '0' + code.toString(); 
      }
      
    }

    let template = code + ' - ' + this.client.firstname + ' ' + this.client.lastname;
    this.clientSearcher.showCustom( template );

  }

  calculateImport( item ) {
    
    if ( item.count || item.count != 0) {
      
      this._items.stock(item.id, { count: item.count }).subscribe( thereStock => {
        
        if( thereStock ) {
          item.import = item.count * item.price;

          this.downpayment = this.calculateDownpayment();
          this.downpaymentBonus = this.calculateDownpaymentBonus();
          this.total = this.calculateTotal();
          
        } else {
          this.noStockSwal.fire();

          if ( item.count == 1) {
            this.removeItem( item );
          } else {
            this.item.count = 1;
            this.calculateImport( item );
          }        
        }

      }); 
      
    } else {
      this.nullCountSwal.fire();
      this.item.count = 1;
      this.calculateImport( item );
    }

  }

  checkStock( item ) {

    if( !this.configuration ) {
      this.configSwal.fire();
      return;
    } 

    this._items.stock( item.id , {}).subscribe( thereStock => {

      if ( thereStock ) {

        for (const it of this.items) {
          if ( item.id == it.id ) {
            this.duplicateItemSwal.fire();
            return;
          }
        }

        item.count = 1;
        item.price = item.price * ( 1 + ( this.configuration.rate * this.configuration.deadline) / 100 )
        item.import = this.item.count * this.item.price;

        this.items.push( item );
        this.itemSearcher.unselect();

        setTimeout(()=>{ // 
          $(this.itemsRows.last.nativeElement).find('input').focus();
        },0);  

      } else {
        this.noStockSwal.fire();
        return;
      }

      this.downpayment = this.calculateDownpayment();
      this.downpaymentBonus = this.calculateDownpaymentBonus();
      this.total = this.calculateTotal();
      
    });
    
  }

  removeItem( item ) {

    for (let i = 0; i < this.items.length; i++) {
      if ( item.id == this.items[i].id ) {
        this.items.splice( i, 1 );
        break;
      }      
    }

    this.downpayment = this.calculateDownpayment();
    this.downpaymentBonus = this.calculateDownpaymentBonus();
    this.total = this.calculateTotal();

  }

  calculateDownpayment(  ) {

    let imp = 0;

    for (const item of this.items) {
      imp += item.import;
    }

    return ( this.configuration.downpayment / 100 ) * imp;
  }

  calculateDownpaymentBonus() {

    return this.downpayment * ( ( this.configuration.rate * this.configuration.deadline ) / 100 );

  }

  calculateTotal() {
    let imp = 0;

    for (const item of this.items) {
      imp += item.import;
    }

    return imp - this.downpayment - this.downpaymentBonus;
  }

  calculateSpotPrice() {

  }

  back() {
    this.router.navigateByUrl('/sales');
  }

  next() {

    if (!this.client || !this.items.length ) {
      this.invalidNextSwal.fire();

      return;
    } 

    this.spotPrice = this.total / ( 1  + (( this.configuration.rate * this.configuration.deadline) / 100));

    this.payments.push({
      countPayments: 3,
      totalToPay: this.spotPrice * (1 + ( this.configuration.rate * 3 / 100)),
      parcialPayment: ( this.spotPrice * (1 + ( this.configuration.rate * 3 / 100)) ) / 3,
      saving:  this.total - this.spotPrice * (1 + ( this.configuration.rate * 3 / 100)),
      selected: false
    },{
      countPayments: 6,
      totalToPay: this.spotPrice * (1 + ( this.configuration.rate * 6 / 100)),
      parcialPayment: ( this.spotPrice * (1 + ( this.configuration.rate * 6 / 100)) ) / 6,
      saving:  this.total - this.spotPrice * (1 + ( this.configuration.rate * 6 / 100)),
      selected: false
    },{
      countPayments: 9,
      totalToPay: this.spotPrice * (1 + ( this.configuration.rate * 9 / 100)),
      parcialPayment: ( this.spotPrice * (1 + ( this.configuration.rate * 9 / 100)) ) / 9,
      saving:  this.total - this.spotPrice * (1 + ( this.configuration.rate * 9 / 100)),
      selected: false
    },{
      countPayments: 12,
      totalToPay: this.spotPrice * (1 + ( this.configuration.rate * 12 / 100)),
      parcialPayment: ( this.spotPrice * (1 + ( this.configuration.rate * 12 / 100)) ) / 12,
      saving:  this.total - this.spotPrice * (1 + ( this.configuration.rate * 12 / 100)),
      selected: false
    });

    this.isSecondStep = true;

  }

  save() {

    if( !this.client || !this.items.length || !this.parcialPayments ) {
      this.invalidNextSwal.fire();
      return;
    }

    let itemsIds = [];

    for (const item of this.items) {
      itemsIds.push(item.id);
    }

    let sale = {
      client: this.client.id,
      downpayment: this.downpayment,
      downpaymentBonus: this.downpaymentBonus,
      total: this.total,
      items: this.items,
      payment: {
        count: this.parcialPayments.countPayments,
        toPayment: this.parcialPayments.parcialPayment,
        totalToPayment: this.parcialPayments.totalToPay,
        saving: this.parcialPayments.saving
      }
    }

    this._sales.create( sale ).subscribe( () => {
      this.successSwal.fire();
      this.router.navigateByUrl('/sales');
    });

  }

  getLastId() {
    this._sales.lastId().subscribe( lastId => { this.code = lastId } );
  }


}
