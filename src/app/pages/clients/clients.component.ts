import { Component, OnInit } from '@angular/core';
import { ClientsService } from 'src/app/services/clients.service';
import { Client } from 'src/app/models/client.model';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  clients: Client[];

  constructor( private _clients: ClientsService) { }

  ngOnInit() {

    this._clients.findAll().subscribe( clients =>  this.clients = clients );

  }

}
