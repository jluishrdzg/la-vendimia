import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientsService } from 'src/app/services/clients.service';
import { Client } from 'src/app/models/client.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-clients-form',
  templateUrl: './clients-form.component.html',
  styleUrls: ['./clients-form.component.scss']
})
export class ClientsFormComponent implements OnInit {

  @ViewChild('saveSwal', {static: false}) private saveSwal: SwalComponent;
  @ViewChild('successSwal', {static: false}) private successSwal: SwalComponent;


  client: Client

  id: number;
  code: number;
  title: string;
  form: FormGroup;
  submitted: boolean = false;

  constructor( 
    private route: ActivatedRoute, 
    private _clients: ClientsService,
    private router: Router
  ) { }

  ngOnInit() {

    this.id = this.route.snapshot.params.id;
    this.form = this.initForm();
    
    if ( this.id ) {
      this.title = 'Actualizar cliente';
      this.code = this.id;
      this.loadClient();
    } else {
      this.title = 'Nuevo Cliente'
      this.getLastId();
    }


  }

  initForm(): FormGroup {
    let patternLetters = /^[a-zA-Z\sáéíóúÁÉÍÓÚñÑ]*$/

    return new FormGroup({
      firstname: new FormControl('', [ Validators.required, Validators.maxLength(100), Validators.pattern(patternLetters) ]),
      lastname: new FormControl('', [ Validators.required, Validators.maxLength(100), Validators.pattern(patternLetters) ]),
      rfc: new FormControl('', [ Validators.required, Validators.maxLength(13) ])
    });
  }

  loadClient() {

    this._clients.findOne( this.id ).subscribe( ( client: any) => {
      delete client.id;
      this.form.setValue( client )
    });

  }

  openSaveSwal() {

    if( this.form.invalid ) return;

    this.saveSwal.fire();
    
  }

  save() {

    if( this.form.invalid ) return;

    if( !this.id ) {

      this._clients.create( this.form.value ).subscribe( client => {
        this.successSwal.fire();
        this.back();
      });

    } else {
      this._clients.update( this.id, this.form.value ).subscribe( result => {
        this.successSwal.fire();
        this.back();
      });
    }

  }

  resetForm() {
    this.form = this.initForm();
  }

  getLastId() {
    this._clients.lastId().subscribe( lastId => { this.code = lastId } );
  }

  back() {
    this.router.navigateByUrl('/clients');
  }

}
