import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsComponent } from './clients.component';
import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsService } from 'src/app/services/clients.service';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ClientsFormComponent } from './clients-form/clients-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidateControlModule } from 'src/app/directives/validate-control/validate-control.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';



@NgModule({
  declarations: [ClientsComponent, ClientsFormComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    PipesModule,
    ReactiveFormsModule,
    ValidateControlModule,
    SweetAlert2Module,
  ], providers: [ClientsService]
})
export class ClientsModule { }
