import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationComponent } from './configuration.component';
import { ConfigurationRoutingModule } from './configuration-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidateControlModule } from 'src/app/directives/validate-control/validate-control.module';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';



@NgModule({
  declarations: [ConfigurationComponent],
  imports: [
    CommonModule,
    ConfigurationRoutingModule,
    ReactiveFormsModule,
    ValidateControlModule,
    SweetAlert2Module
  ],
  providers: [ConfigurationService]
})
export class ConfigurationModule { }
