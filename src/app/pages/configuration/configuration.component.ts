import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  @ViewChild('saveSwal', {static: false}) private saveSwal: SwalComponent;
  @ViewChild('successSwal', {static: false}) private successSwall: SwalComponent;

  isFirstRegister: boolean = true;
  form: FormGroup;

  constructor( private _configuration: ConfigurationService ) { }

  ngOnInit() {

    this.loadConfiguration();
    this.form = this.initForm();

  }

  loadConfiguration() {

    this._configuration.findOne( 1 ).subscribe( (configuration: any) => {
      if ( configuration ) {
        delete configuration.id;
        this.form.setValue( configuration );
        this.isFirstRegister = false;
      }
    });

  }

  initForm(): FormGroup {
    let patternNumbers = /^[0-9]*([.][0-9]{1,2})?$/;

    return new FormGroup({
      rate: new FormControl('', [ Validators.required, Validators.pattern(patternNumbers) ]),
      downpayment: new FormControl('', [ Validators.required, Validators.pattern(patternNumbers) ]),
      deadline: new FormControl('', [ Validators.required, Validators.pattern(patternNumbers) ]),
    });
  }

  openSaveAlert() {

    if( this.form.invalid ) return;

    this.saveSwal.fire();

  }

  save() {
 
    if( this.form.invalid ) return;

    if( this.isFirstRegister ) {

      this._configuration.create( this.form.value ).subscribe( item => {
        this.isFirstRegister = false;
        this.successSwall.fire();
      });

    } else {
      this._configuration.update( 1, this.form.value ).subscribe( result => {
        this.successSwall.fire();
      });
    }

  }


}
