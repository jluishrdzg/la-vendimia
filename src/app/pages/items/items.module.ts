import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsComponent } from './items.component';
import { ItemsRoutingModule } from './items-routing.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ItemsFormComponent } from './items-form/items-form.component';
import { ValidateControlModule } from 'src/app/directives/validate-control/validate-control.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';



@NgModule({
  declarations: [ItemsComponent, ItemsFormComponent],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    PipesModule,
    ReactiveFormsModule,
    ValidateControlModule,
    SweetAlert2Module
  ]
})
export class ItemsModule { }
