import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ItemsService } from 'src/app/services/items.service';
import { Item } from 'src/app/models/item.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-items-form',
  templateUrl: './items-form.component.html',
  styleUrls: ['./items-form.component.scss']
})
export class ItemsFormComponent implements OnInit {

  @ViewChild('saveSwal', {static: false}) private saveSwal: SwalComponent;
  @ViewChild('successSwal', {static: false}) private successSwal: SwalComponent;

  item: Item;

  id: number;
  code: number;
  title: string;
  form: FormGroup;
  submitted: boolean = false;

  constructor( 
    private route: ActivatedRoute, 
    private _items: ItemsService,
    private router: Router
  ) { }

  ngOnInit() {

    this.id = this.route.snapshot.params.id;
    this.form = this.initForm();
    
    if ( this.id ) {
      this.title = 'Actualizar Artículo';
      this.code = this.id;
      this.loadItem();
    } else {
      this.title = 'Nuevo Artículo'
      this.getLastId();
    }

  }

  initForm(): FormGroup {
    let patternStock = /^[0-9]\d*$/;
    let patternPrice = /^[0-9]*([.][0-9]{1,2})?$/;

    return new FormGroup({
      description: new FormControl('', [ Validators.required, Validators.maxLength(500) ]),
      model: new FormControl('', [ Validators.required, Validators.maxLength(100)  ]),
      price: new FormControl('', [ Validators.required, Validators.maxLength(100), Validators.pattern(patternPrice) ]),
      stock: new FormControl('', [ Validators.required, Validators.maxLength(13), Validators.pattern(patternStock) ])
    });
  }

  loadItem() {

    this._items.findOne( this.id ).subscribe( (item: any ) => {
      delete item.id
      this.form.setValue( item )
    });

  }

  openSaveSwal() {

    if( this.form.invalid ) return;

    this.saveSwal.fire();

  }

  save() {

    if( this.form.invalid ) return;

    if( !this.id ) {

      this._items.create( this.form.value ).subscribe( item => {
        this.successSwal.fire();
        this.back();
      });

    } else {
      this._items.update( this.id, this.form.value ).subscribe( result => {
        this.successSwal.fire();
        this.back();
      });
    }

  }

  resetForm() {
    this.form = this.initForm();
    
  }

  getLastId() {
    this._items.lastId().subscribe( lastId => { this.code = lastId } );
  }

  back() {
    this.router.navigateByUrl('/items');
  }

}
