import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemsComponent } from './items.component';
import { ItemsFormComponent } from './items-form/items-form.component';


const routes: Routes = [
    { path: '', component: ItemsComponent },
    { path: 'new', component: ItemsFormComponent },
    { path: ':id', component: ItemsFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }