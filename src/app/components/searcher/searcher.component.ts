import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnChanges, Input, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { EventEmitter } from '@angular/core';

declare const $: any;

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss'],
})
export class SearcherComponent implements OnInit, AfterViewInit {

  @ViewChild('searcher', { static: false }) searcher: ElementRef;
  @ViewChild('results', { static: false}) results: ElementRef;

  @Input('url') url: string;
  @Input('keys') keys: string[];

  @Output('onSelect') onSelect = new EventEmitter();
  @Output('onCancel') onCancel = new EventEmitter();


  query: string = '';
  events: Event[];

  showResults: boolean = false;
  loading: boolean = false;

  searchSub: Subscription;

  data: any;
  selectedItem: any;
  template: any;

  removeCancelBoton = false;

  constructor( private http: HttpClient ) { }

  ngOnInit() {}

  search() {

    this.setPosition();

    if ( this.searchSub ) {
      this.searchSub.unsubscribe();
    }

    if ( this.query.length < 3) {
      this.data = [];
      this.loading = false;
      return;
    }

    this.loading = true;

    let params: any = {};

    if ( this.query ) {
      params.query = this.query;
    }

    this.http.get(environment.server + this.url, { params }).subscribe( data => {
      this.data = data;
      this.loading= false;
    });

  }

  delayHide() {
    setTimeout(() => {
      this.showResults = false;
    }, 200)
  }

  ngAfterViewInit(): void {

    this.setPosition();

  }

  setPosition() {
    let width = this.searcher.nativeElement.offsetWidth;

    $( this.results.nativeElement ).width(width);

  }

  selectItem( item ) {
    this.selectedItem = item;
    this.template = item[this.keys[0]];

    this.onSelect.emit(item);
  }

  showCustom( template ) {

    this.template = template;

  }

  unselect() {
    this.query = ''; 
    this.selectedItem = null; 
    this.data=[];
  }


}
