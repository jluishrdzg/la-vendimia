import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearcherComponent } from './searcher/searcher.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
    SearcherComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule
  ],
  exports: [
    SearcherComponent
  ]
})
export class ComponentsModule { }
