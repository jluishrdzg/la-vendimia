import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'clave'
})
export class ClavePipe implements PipeTransform {

  transform(value: string | number, ...args: any[]): string {

    if( !value ) return '';

    value = value.toString();

    if( value.length < 4 ) {

      let ceros = 4 - value.length;

      for (let i = 0; i <  ceros; i++) {
        value = '0' + value.toString(); 
      }
      
    }
    
    return value;
  }

}
