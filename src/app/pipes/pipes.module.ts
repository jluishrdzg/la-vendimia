import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClavePipe } from './clave.pipe';
import { HighlightPipe } from './highlight.pipe';



@NgModule({
  declarations: [
    ClavePipe,
    HighlightPipe
  ],
  imports: [
    CommonModule
  ], 
  exports: [
    ClavePipe,
    HighlightPipe
  ]
})
export class PipesModule { }
