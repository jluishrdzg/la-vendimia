import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlight'
})
export class HighlightPipe implements PipeTransform {

  transform(value: string, highlight: string): string {

    let lval = value.toLocaleLowerCase();
    let lhigh = highlight.toLocaleLowerCase();

    const index = lval.indexOf(lhigh);

    if ( index >= 0 ) {

      let head = value.substring(0, index);
      let body = '<span class="highlight">' + value.substring(index, index + lhigh.length) + '</span>';
      let tail = value.substring(index + lhigh.length);

      value = ' ' + head + body + tail + ' ';

    }

    return value;
  }

}
