import { Directive, OnInit, Input } from '@angular/core';
import { AbstractControl, ControlContainer, FormGroupDirective } from '@angular/forms';
import { Observable } from 'rxjs';
import { merge} from 'rxjs'
import { of } from 'rxjs'
import { map } from 'rxjs/operators'

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[validate-control]'
})
export class ValidateControlDirective implements OnInit {

  // tslint:disable-next-line:no-input-rename
  @Input('validate-control') controlName: string;
  control: AbstractControl;
  hasView = false;
  controlValue$: Observable<any>;

  _form;
  constructor(
    private _fg: ControlContainer
  ) { }

  ngOnInit() {
    this.control = this.form.get(this.controlName);

    this._form = ( <FormGroupDirective>this._fg );

    let formSubmit$ = this._form.ngSubmit.pipe( map ( () => { return { isSubmit: true } }));

    this.controlValue$ =  merge(this.control.valueChanges, of(''), formSubmit$ );
  }

  match(error: string, forcedMatch?: boolean) {

    if ( forcedMatch ) {

      if ( this.control && this.control.errors ) {

        if (Object.keys(this.control.errors).indexOf(error) > -1) {
          return true;
        }

      }


    } else if ( this.control && this.control.errors && !this.control.pristine) {

      if (Object.keys(this.control.errors).indexOf(error) > -1) {
        return true;
      }

    }

    return false;
  }

  getControl() {
    return this.control;
  }

  get form() { return this._fg.formDirective ? (this._fg.formDirective as FormGroupDirective).form : null; }

}
