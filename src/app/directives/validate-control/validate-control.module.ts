import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidateControlDirective } from './validate-control.directive';
import { ValidateMessageDirective } from './validate-message.directive';

@NgModule({
    declarations: [
        ValidateControlDirective,
        ValidateMessageDirective
    ],
    imports: [ CommonModule ],
    exports: [
        ValidateControlDirective,
        ValidateMessageDirective
    ],
    providers: [],
})
export class ValidateControlModule {}
