import { Directive, OnInit, Input, TemplateRef, ViewContainerRef, OnDestroy } from '@angular/core';
import { ValidateControlDirective } from './validate-control.directive';
import { Subscription } from 'rxjs';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[validate-message]'
})
export class ValidateMessageDirective implements OnInit, OnDestroy {

  // tslint:disable-next-line:no-input-rename
  @Input('validate-message') type: string;

  private hasView = false;
  private sub: Subscription

  constructor(
    private invalidmessage: ValidateControlDirective,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  ngOnInit() {

   this.sub = this.invalidmessage.controlValue$.subscribe( ( event ) => {
      let forceMatch = event && event.isSubmit  ? true : false;
      this.setVisible( forceMatch );
    });

  }

  private setVisible( forceMatch ) {

    if (this.invalidmessage.match(this.type, forceMatch ) ) {

      if (!this.hasView) {

        this.viewContainer.createEmbeddedView(this.templateRef);
        this.hasView = true;

      }
    } else {

      if (this.hasView) {

         this.viewContainer.clear();
         this.hasView = false;

      }
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
