export class Item {
    id?: number;
    description: string;
    model: string;
    price: number;
    stock: number;

    count?: number;
    import?: number;
}