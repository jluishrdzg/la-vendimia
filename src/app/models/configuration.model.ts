export class Configuration {
    id?: number;
    rate: number;
    downpayment: number;
    deadline: number;
}