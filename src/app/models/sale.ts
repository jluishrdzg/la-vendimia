import { Item } from './item.model';

export class Sale {
    id?: number;
    client: number;
    downpayment: number;
    downpaymentBonus: number;
    total: number;
    date?: string;
    items?: Item[];
}