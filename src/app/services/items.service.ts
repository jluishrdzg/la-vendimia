import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Item } from 'src/app/models/item.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  private api = environment.server + '/items'

  constructor( private http: HttpClient ) { }

  findAll(): Observable<Item[]> {
    return this.http.get<Item[]>( this.api );   
  }

  findOne( id: number ) {
    let api = this.api +'/' + id;
    return this.http.get( api );   
  }

  create( item: Item ) {
    return this.http.post(this.api, item);
  }

  update( id: number, item: Item ) {
    let api = this.api +'/' + id;
    return this.http.put( api, item );
  }

  delete(id: number) {
    let api = this.api +'/' + id;
    return this.http.delete( api );
  }

  lastId() {
    let api = this.api +'/lastid';
    return this.http.get<number>( api );
  }

  stock( id: number, params ) {
    let api = this.api +'/stock/' + id;
    return this.http.get( api, { params } );   
  }


}
