import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Item } from 'src/app/models/item.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private api = environment.server + '/configuration'

  constructor( private http: HttpClient ) { }

  findOne( id: number ) {
    let api = this.api +'/' + id;
    return this.http.get( api );   
  }

  create( item: Item ) {
    return this.http.post(this.api, item);
  }

  update( id: number, item: Item ) {
    let api = this.api +'/' + id;
    return this.http.put( api, item );
  }

}
