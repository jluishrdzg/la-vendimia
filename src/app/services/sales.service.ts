import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sale } from '../models/sale';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  private api = environment.server + '/sales'

  constructor( private http: HttpClient ) { }

  findAll(): Observable<Sale[]> {
    return this.http.get<Sale[]>( this.api );   
  }

  create( sañe: Sale ) {
    return this.http.post(this.api, sañe);
  }

  lastId() {
    let api = this.api +'/lastid';
    return this.http.get<number>( api );
  }

}
