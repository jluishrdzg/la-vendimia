import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Client } from 'src/app/models/client.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  private api = environment.server + '/clients'

  constructor( private http: HttpClient ) { }

  findAll(): Observable<Client[]> {
    return this.http.get<Client[]>( this.api );   
  }

  findOne( id: number ) {
    let api = this.api +'/' + id;
    return this.http.get( api );   
  }

  create( client: Client ) {
    return this.http.post(this.api, client);
  }

  update( id: number, client: Client ) {
    let api = this.api +'/' + id;
    return this.http.put( api, client );
  }

  delete(id: number) {
    let api = this.api +'/' + id;
    return this.http.delete( api );
  }

  lastId() {
    let api = this.api +'/lastid';
    return this.http.get<number>( api );
  }


}
